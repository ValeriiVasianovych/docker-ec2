# Docker Deployment Pipeline

This repository contains a CI/CD pipeline for deploying Docker image. The pipeline consists of two stages:

## Preview
![Preview](preview.png)

1. **docker-build**:
   - This stage builds a Docker image using the latest Docker version.
   - It logs in to the Docker registry using credentials provided via CI variables.
   - After logging in, it builds the Docker image using the Dockerfile in the repository.
   - This stage is triggered only for changes in the `main` branch.

2. **run-deploy**:
   - The Docker image is started as a container, exposed on port 80.
   - After starting the container, it pushes the image to the Docker registry.
   - Finally, it stops all running Docker containers.
   - Like the previous stage, this one is also triggered only for changes in the `main` branch.

To use this pipeline, ensure that your Dockerfile is configured correctly and your Docker registry credentials are set up in your CI environment.
